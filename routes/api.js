/*
 * @author wingbot.ai
 */
'use strict';

const express = require('express');
const {
    GraphApi, validateBotApi, postBackApi, conversationsApi
} = require('wingbot');

const config = require('../config');
const wrapRoute = require('../lib/wrapRoute');

const botConfigStorage = require('../lib/botConfigStorage');
const stateStorage = require('../lib/stateStorage');
const { botFactory, channel, botSettings } = require('../bot');

const DEFAULT_ACCESS_GROUPS = ['botEditor', 'botAdmin', 'appToken'];
const BOT_UPDATE_GROUPS = ['botEditor', 'botAdmin', 'botUser'];
const POSTBACK_GROUPS = ['appToken'];

const api = new GraphApi([
    postBackApi(channel, POSTBACK_GROUPS),
    conversationsApi(stateStorage, null, null, BOT_UPDATE_GROUPS),
    validateBotApi(botFactory, 'start', 'foobar', BOT_UPDATE_GROUPS),
    botConfigStorage.api(() => botSettings(), BOT_UPDATE_GROUPS)
], {
    token: config.wingbot.token,
    appToken: config.appToken,
    groups: DEFAULT_ACCESS_GROUPS
});


module.exports = [
    express.json({ limit: config.expressBodyLimit }),
    wrapRoute(async (req, res) => {
        const { body } = req;
        const response = await api.request(body, req.headers);

        res.set({
            'Content-Type': 'application/json'
        })
            .send(response);
    })
];
