/*
 * @author wingbot.ai
 */
'use strict';

const createError = require('http-errors');
const analytics = require('universal-analytics');
const { replaceDiacritics } = require('webalize');
const wrapRoute = require('../lib/wrapRoute');
const config = require('../config');

function trackEvent (sender, text, url) {
    const tracker = analytics(config.gaCode, sender, { strictCidFormat: false });

    const word = replaceDiacritics(text).replace(/\s+/g, ' ').toLowerCase().trim();

    tracker.event('User: Button - url', url, word);

    return new Promise(r => tracker.send(r));
}

module.exports = [
    wrapRoute(async (req, res) => {
        const { url = null, text = null, sender = null } = req.query || {};

        if (!url || !text || !sender) {
            throw createError(404);
        }

        await trackEvent(sender, text, url);

        res.redirect(301, url);
    })
];
