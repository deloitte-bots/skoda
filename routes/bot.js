/*
 * @author wingbot.ai
 */
'use strict';

const express = require('express');
const log = require('../lib/log');
const config = require('../config');

const wrapRoute = require('../lib/wrapRoute');
const { channel, processor } = require('../bot');


module.exports = [
    express.json({
        verify: (req, res, buf) => {
            req.rawBody = buf;
        },
        limit: config.expressBodyLimit
    }),
    wrapRoute(async (req, res) => {
        if (req.method === 'GET') {
            if (req.query.ref) {
                res.redirect(`/?ref=${encodeURIComponent(req.query.ref)}`);
                return;
            }
            res.send('RUNNING');
            return;
        }
        await channel.verifyRequest(req.rawBody, req.headers);
        channel.processEvent(req.body)
            .catch((e) => {
                log.error('Request failed', e);
            });
        res.send('OK');
    })
];
