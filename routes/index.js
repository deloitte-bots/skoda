/*
 * @author wingbot.ai
 */
'use strict';

const { Router } = require('express');

const bot = require('./bot');
const tracker = require('./tracker');
const api = require('./api');

const app = Router();

app.use('/bot', ...bot);
app.post('/api', ...api);
app.get('/tracker', ...tracker);

module.exports = app;
