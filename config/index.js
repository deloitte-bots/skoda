/*
 * @author wingbot.ai
 */
'use strict';

const config = {

    environment: process.env.NODE_ENV || 'development',

    isProduction: false,

    prefix: process.env.PREFIX || 'skoda-development',

    // where the assets or html views are stored
    appUrl: 'http://localhost:3000',

    // where the application API lays
    apiUrl: 'http://localhost:3000',

    expressBodyLimit: '5mb',

    botService: {
        appId: process.env.BOT_APP_ID || 'a9f24ec8-34c9-4b55-9640-4dc2429f0250',
        appSecret: process.env.BOT_APP_PASSWORD || '9i37Vn_2IbjFsLdci~9Ga~RAC6S_tezNxo'
    },

    wingbot: {
        bot: 'skoda',
        botId: '6757e0fc-1504-4ac4-9bc9-fa2c10268f96',
        snapshot: 'development',
        token: 'UTs0OEhXdtNDJRO8TLmn23Zbu8buCtsLRdhtAOXbKHOBrZ3Zr5a5SZIC0b11cmXSuXEfB1WK81wTWaJn4bweBKJJVCaNjyTCxeXjx7S5QqXxKMdsi5tK850KA5bKtyN3',
        ai: 'skoda-development'
    },

    db: {
        url: process.env.MONGODB_CONNECTION_STRING || 'mongodb://127.0.0.1:27017/skoda',

        db: 'skoda',

        options: {
            poolSize: 3,
            autoReconnect: true,
            useNewUrlParser: true,
            haInterval: 5000,
            reconnectTries: 1200,
            connectTimeoutMS: 5000,
            socketTimeoutMS: 12000,
            reconnectInterval: 500
        }
    },


    appInsights: process.env.APPINSIGHTS_INSTRUMENTATIONKEY
        || process.env.APPSETTING_APPINSIGHTS_INSTRUMENTATIONKEY,

    gaCode: ''
};

/**
 * initialize config file
 *
 * @param {Object} cfg
 * @param {string} env
 */
function initialize (cfg, env = 'development') {
    try {
        const configuration = module.require(`./config.${env}`);

        // deeper object assign
        Object.keys(configuration)
            .forEach((key) => {
                if (typeof cfg[key] === 'object'
                    && typeof configuration[key] === 'object') {

                    Object.assign(cfg[key], configuration[key]);
                } else {
                    Object.assign(cfg, { [key]: configuration[key] });
                }
            });
    } catch (e) {
        console.info(`No configuration for ENV: ${env}`); // eslint-disable-line
    }

    return cfg;
}

initialize(config, process.env.NODE_ENV);

module.exports = config;
