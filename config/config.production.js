/*
 * @author wingbot.ai
 */
'use strict';

module.exports = {

    isProduction: true,

    // where the assets or html views are stored
    appUrl: 'https://skodabot.azurewebsites.net',

    // where the application API lays
    apiUrl: 'https://skodabot.azurewebsites.net',


    wingbot: {
        snapshot: 'production',
        token: 'eyzxkFT1NVXsTQqWh27Ha2SnQUbv9f5ndkkwO78u8E1sQHAd1k3x57z3vXfXZXH0UflW0dmI1aiTkSdfx4EeApajRzZmL2pcR6eWIpKilM0cG2DVyUu7viaWWVVMj3TF',
        ai: 'skoda-production'
    },

    // token for accessing a chatbot API
    appToken: '34e5acc0a904207c154fd6c13969b4acb7620bebce2f2011b515b36d2b57b008f14ac4c492236d9f6028b0f09266323b58ec55104bbccdd9a1149ad60b093c246bb58d5d0dbeeb360ec59d329f97b752',

    botService: {
        appId: process.env.BOT_APP_ID || 'a9f24ec8-34c9-4b55-9640-4dc2429f0250',
        appSecret: process.env.BOT_APP_PASSWORD || '9i37Vn_2IbjFsLdci~9Ga~RAC6S_tezNxo'
    },

    db: {
        db: 'skoda',
        url: 'mongodb+srv://wingbot:wingbot@cluster0.vq9za.mongodb.net/skoda?retryWrites=true&w=majority'
    },

    gaCode: 'UA-107706505-1'

};
