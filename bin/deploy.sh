echo Installing NPM packages...
npm install

if [ -f skoda.zip ]
then
    echo ZIP package exists, deleting.
    rm skoda.zip
fi

echo Creating ZIP package...
npm run pack

echo Creating resource group...
az group create -n "skoda-rg" -l "northeurope"

echo Creating resources...
az group deployment create -g "skoda-rg" --template-file .\deployment\template.json --parameters ..\deployment\parameters.json

echo Deploying...
az webapp deployment source config-zip -g "skoda-rg" -n "skoda" --src ".\skoda.zip"

echo Done.
