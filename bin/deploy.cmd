@echo off
echo Installing NPM packages...
call npm install

if exist ".\skoda.zip" (
    echo ZIP package exists, deleting.
    del ".\skoda.zip"
)

echo Creating ZIP package...
call npm run pack

echo Creating resource group...
call az group create -n "skoda-rg" -l "northeurope"

echo Creating resources...
call az group deployment create -g "skoda-rg" --template-file .\deployment\template.json --parameters ..\deployment\parameters.json

echo Deploying...
call az webapp deployment source config-zip -g "skoda-rg" -n "skoda" --src ".\skoda.zip"

echo Done.
