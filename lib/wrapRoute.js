/*
 * @author wingbot.ai
 */
'use strict';

function wrapRoute (fn) {
    return (req, res, next) => {
        Promise.resolve(fn(req, res, next))
            .catch(next);
    };
}

module.exports = wrapRoute;
