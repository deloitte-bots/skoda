/*
 * @author wingbot.ai
 */
'use strict';

const { BotConfigStorage } = require('wingbot-mongodb');
const mongodb = require('./mongodb');

module.exports = new BotConfigStorage(mongodb, 'botconfig');
