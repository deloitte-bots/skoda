/**
 * @author wingbot.ai
 */
'use strict';

const config = require('../../config');
const loggerOveridderFactory = require('./loggerOveridderFactory');

let logger = console;


if (config.appInsights) {
    // @ts-ignore
    const appInsights = module.require('applicationinsights');

    appInsights.setup(config.appInsights).start();

    // @ts-ignore
    logger = loggerOveridderFactory((obj, exception) => {
        const {
            level, message, stack, args
        } = obj;

        if (exception) {
            appInsights.defaultClient.trackException({
                exception
            });
        }

        appInsights.defaultClient.trackEvent({
            name: `${level}: ${message}`,
            properties: {
                level,
                stack,
                args: typeof args === 'object'
                    ? JSON.stringify(args, null, 2)
                    : args
            }
        });

    });

    // @ts-ignore
    logger.sendAndClose = () => {};
} else {
    logger.sendAndClose = () => {};
}

// overide logger here
module.exports = logger;
